#this program will guess user's birthday with 5 tries
from random import randint

name = input("Hi! What is your name? ")
print(f"{name}, I'm going to try to guess your birthday.")

for num in range(5):
    month = int(randint(1,12))
    year = int(randint(1924,2004))
    count = num +1
    guess = input(f"Guess {count} : {name} were you born in {month} / {year}? \n'yes or no.' ").lower()

    if guess == "yes" or guess == "y":
        print("I knew it!")
        exit()
    elif guess == ("no" or guess != "yes") or count != 5:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
        exit()
 
    
    